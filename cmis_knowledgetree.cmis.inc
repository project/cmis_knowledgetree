<?php

/**
 * Implementation of cmisapi_getRepositoryInfo method 
 *
 * @return stdClass $repository repository information object
 * 
 * @todo populate $repository_info with all properties
 */
function cmis_knowledgetree_cmisapi_getRepositoryInfo() {
  module_load_include('utils.inc', 'cmis_knowledgetree');
  // get repo info
  $response = cmis_knowledgetree_utils_invoke_service('servicedocument');
  // register ns
  $repo_info = cmis_utils_process_CMIS_xml($response, '/app:service/app:workspace/cmis:repositoryInfo');
  // initialize repo info object
  $repository = new stdClass();
  // extract repo info from response
  if (false != $repo_info) {
    $repository->repositoryId = cmis_utils_CMIS_xml_get_value($repo_info[0], 'cmis:repositoryId');
    $repository->repositoryName = cmis_utils_CMIS_xml_get_value($repo_info[0], 'cmis:repositoryName');
    $repository->repositoryDescription = cmis_utils_CMIS_xml_get_value($repo_info[0], 'cmis:repositoryDescription');
    $repository->vendorName = cmis_utils_CMIS_xml_get_value($repo_info[0], 'cmis:vendorName');
    $repository->productName = cmis_utils_CMIS_xml_get_value($repo_info[0], 'cmis:productName');
    $repository->productVersion = cmis_utils_CMIS_xml_get_value($repo_info[0], 'cmis:productVersion');
    $repository->rootFolderId = (string) cmis_utils_CMIS_xml_get_value($repo_info[0], 'cmis:rootFolderId');
  }

  return $repository;
}

/**
 * Implementation of cmisapi_getChildren method
 * @param $repositoryId
 * @param $folderId
 */
function cmis_knowledgetree_cmisapi_getChildren($repositoryId, $folderId) {
  module_load_include('utils.inc', 'cmis_knowledgetree');

  $folderId = cmis_knowledgetree_objectId($folderId);
  
  if ($folderId['noderef_url']) {
    $url = $folderId['noderef_url'];
  }
  else {
    $url = $folderId['url'];
  }
  $xml = cmis_knowledgetree_utils_invoke_service($url . '/children');

  if (false != $xml) {
    return cmis_knowledgetree_getEntries(cmis_utils_process_CMIS_xml($xml, '//D:entry'));
  }

    return false;
}

/**
 * Implementation of cmisapi_createDocument method 
 * @param $repositoryId
 * @param $objectTypeId
 * @param $properties
 * @param $folderId
 * @param $content
 * @param $versioningState
 */
// NOTE this function is currently broken
// TODO fix
function cmis_knowledgetree_cmisapi_createDocument($repositoryId, $objectTypeId = 'document', $properties = array (), 
                                                   $folderId = null, $content = null, $versioningState = null)
{
  module_load_include('utils.inc', 'cmis_knowledgetree');

  $parentFolderId = cmis_knowledgetree_objectId($folderId);
  if (!is_array($parentFolderId) || !array_key_exists('noderef_url', $parentFolderId)) {
    drupal_set_message('Unable to find destination folder: ' . $folderId, 'error');
    return false;
  }

  $postvars = '<?xml version="1.0" encoding="utf-8"?>' .
  '<entry xmlns="http://www.w3.org/2005/Atom" xmlns:cmis="http://www.cmis.org/2008/05">' .
  '<title>' . $properties['title'] . '</title>' .
  '<summary>' . $properties['summary'] . '</summary>' .
  '<content type="' . $properties['content-type'] . '">' . base64_encode($content) . '</content>' .
  '<cmis:object>' .
  '<cmis:properties>' .
  '<cmis:propertyString cmis:name="ObjectTypeId">' .
  '<cmis:value>' . $objectTypeId . '</cmis:value>' .
  '</cmis:propertyString>' .
  '</cmis:properties>' .
  '</cmis:object>' .
  '</entry>';

  $header[] = 'Content-type: application/atom+xml;type=entry';
  $header[] = 'Content-length: ' . strlen($postvars);
  $header[] = 'MIME-Version: 1.0';

  $xml = cmis_knowledgetree_utils_invoke_service($parentFolderId['noderef_url'] . '/children', 'basic', $header, 'CUSTOM-POST', $postvars);
  $entry = cmis_utils_process_CMIS_xml($xml, '/D:entry');
  $entry = $entry[0][0];

  $objectId = cmis_knowledgetree_objectId((string)$entry->id);

  return $objectId['noderef'];
}

// NOTE this function does not currently work
// TODO fix
/**
 * Implementation of cmisapi_createDocument method
 * @param string $repositoryId
 * @param string $objectTypeId [optional]
 * @param array $properties [optional]
 * @param string $folderId [optional]
 * @return 
 */
function cmis_knowledgetree_cmisapi_createFolder($repositoryId, $objectTypeId = 'folder', $properties = array (), $folderId = null)
{
  module_load_include('utils.inc', 'cmis_knowledgetree');

  $parentFolderId = cmis_knowledgetree_objectId($folderId);
  if (!is_array($parentFolderId) || !array_key_exists('noderef_url', $parentFolderId)) {
    drupal_set_message('Unable to find destination folder: ' . $folderId, 'error');
    return false;
  }

  $postvars = '<?xml version="1.0" encoding="utf-8"?>'
            . '<entry xmlns="http://www.w3.org/2005/Atom" xmlns:cmis="http://www.cmis.org/2008/05">'
            . '<title>' . $properties['title'] . '</title><summary>' . $properties['summary'] . '</summary>'
            . '<cmis:object><cmis:properties><cmis:propertyString cmis:name="ObjectTypeId">'
            . '<cmis:value>' . $objectTypeId . '</cmis:value></cmis:propertyString></cmis:properties>'
            . '</cmis:object></entry>';

  $header[] = 'Content-type: application/atom+xml;type=entry';
  $header[] = 'Content-length: ' . strlen($postvars);
  $header[] = 'MIME-Version: 1.0';

  $xml = cmis_knowledgetree_utils_invoke_service($parentFolderId['noderef_url'] . '/children', 'basic', $header, 'CUSTOM-POST', $postvars);
  $entry = cmis_utils_process_CMIS_xml($xml, '/D:entry');
  $entry = $entry[0][0];

  $objectId = cmis_knowledgetree_objectId((string)$entry->id);

  return $objectId['noderef'];
}

/**
 * Implementation of cmisapi_getProperties method
 * @param $repositoryId
 * @param $objectId
 */
function cmis_knowledgetree_cmisapi_getProperties($repositoryId, $objectId)
{
  module_load_include('utils.inc', 'cmis_knowledgetree');

  $objectId = cmis_knowledgetree_objectId($objectId);

  $temp = new stdClass();
  $temp->id = $objectId;

  return $temp;
}